# iampatch

Toolset for I Am Future Demo enhancement.

Have you played it all? Are you already Future? Be more Future! With this handy tool of yours you can expand your experience beyond any boundaries.

**Warning : Demo game at some locations, that were previously inaccessible, has NO BOUDARY boxes for railings and holes (because they were not needed). So nothing stopping you from falling off of building. In case you do fall off or into a building, use PATCH_location.bat script to reset your position ;)**

**Warning : Always backup your savefile (for location : see bottom of this page)**

**Warning : After using this tool, it would probably be good idea not to report any strangeness as bugs - things there are still in making.**

## What it do ?

- PATCH_tools.bat - upgrades your tools to max level
- PATCH_location.bat - reset your position to starting location
- PATCH_fertilizers.bat - add fertilizers into inventory (they cannot be crafted in demo, so I added this tool for convenience)
- sh.bat - opens terminal with all useful pathes in PATH environment variable

## How it does it ?

Program search for save file in "%APPDATA%/../LocalLow/Mandragora/I Am Future/SaveData/Story.demo.json" location, then modifies it.

If presumed savefile location (C:/Users/[your-user-name]/AppData/LocalLow/Mandragora/I Am Future/SaveData/Story.demo.json) is not found, tools won't work.

You can verify your instalation by running sh.bat and entering "iampatch -i" (without quotes). It shouls load savefile and dump your inventory info on screen.
